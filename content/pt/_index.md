---
title: "Bruno Cardoso"

description: "A Hugo site built with GitLab Pages"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---


Estudante de Enginharia Electrotécnica e Computadores, 


#### -Projectos pessoais:
* Computador de 16 bits
Projecto de paixão, baseado em termos gerais no trabalho de James Sharman,
Mantendo a estrutura geral mas mudando cada bloco bastante e adicionando mais
funcionabilidade.

#### -Projectos ligados á universidade:
* Para Projecto 1
* Redes de computadores 














This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
and [Hugo](https://gohugo.io), and can be built in under 1 minute.
Literally. It uses the [`ananke` theme](https://github.com/theNewDynamic/gohugo-theme-ananke)
which supports content on your front page.
Edit `/content/en/_index.md` to change what appears here. Delete `/content/en/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
