---
date: 2022-06-16T10:58:08-04:00
description: "Logica TTL embora simples permite fazer bastante basta só saber o que se quer criar."
featured_image: "/images/Pope-Edouard-de-Beaumont-1844.jpg"
tags: ["scene"]
title: "Computador de 16 bits"
---


Architectura de computadores é uma area de interesse já a algum tempo, e desde que vi
os videos de [Ben Eater](https://eater.net/) que quis montar o meu proprio simples computador
inicialmente planeava apenas fazer uma versão da arquitetura dele com mais memoria mais registers e por ser louco por velocidade a serie 74F ou Fast TTL, mas entretanto descobri [James Bates](https://www.youtube.com/user/jimmyb998) que aumentou a complexidade dos planos ligeiramente. Enquanto esperava por algumas peças para terminar o computador, encontrei [Bill Buzbee](http://www.homebrewcpu.com/) que tinha feito um computador poderoso o sufeciente para correr um sistema operativo baseado em Unix chamado Minix, acesso á internet, permite controlo via telnet, etc, super impressionante, mas embora a architectura esteja explicada e obviamente bastante poderosa, não gostei do fluxo de dados que tinha e achava demasiado complexa para conseguir entender tudo.
  Por fim encontrei [James Sharman](https://www.youtube.com/c/weirdboyjim) que tem hoje cerca de 100 videos sobre o computador que está a montar, é de 8 bits, como os outros mas tem uma grande differença, ele implementou uma pipeline de 2 estagios, que faz com que a performance
  seja bem proxima de 1 instrução por clock, muito à frente dos outros que fazem perto de 1/10 disso. Graças ao enorme detalhe dos seus videos a idea de fazer um computador muito mais complexo passou a ser possivel e com a ajuda do programa de simulação logica [Digital](https://github.com/hneemann/Digital) deixou de ser preciso ter todos os componentes e longas montagems em breadboards, eu podia rapidamente desenvolver o design em simulador testa-lo e passar directamente para a realidade apenas depois de ter tudo idializado.

# Arquitetura

Mantive a mesma arquitetura geral do James, mas em cada bloco modifiquei ao maximo,
### base - Chips usados

  Em 1966 Texas Instruments lançou a serie de chips logicos SN7400 que rapidamente se tornou num fenomeno sucesso de tal modo que ainda hoje chips são feitos baseados nessa serie, esta usava logica transistor transistor (TTL), ao longo dos anos multiplas versões desta serie foram feitas, umas reduzindo o consumo de energia, outras permitindo mais performance, de destaque a serie LS (schottky de baixo consumo) o tipo mais popular com consumo mais baixo que a serie original, HC (usando CMOS de alta velocidade) lançados nos anos 80, a mais importante para este artigo a serie Fast (74F) lançada em 1978, a versão mais rapida (das de 5 voltes).
James usou a versão HCT (o T significa que supporta os niveis de tensão TTL), uma familia que usa muito menos corrente por usar mosfets, eu quis focar-me em maximizar o performance por isso, em troca de muito mais corrente usei a familia Fast, baseada em TTL que não só tem tempos de propagação bastante mais baixos, tambem tem tempos de ascensão na gama de ~1-2 ns, um exemplo da diferença 69 ns para os 74HCT283, a versão Fast 74F283 tem tempos de propagação de apenas 10.5 ns.

### Precisão base

Passei de 8 bits para 16 bits, isto embora signifique que tenha que duplicar o numero de chips usados, 16 bits significa muito mais precisão e significa muito menos necessidade de aumentar a precisão usando multiplas words.

### Pipeline

No cpu do James ele tem 2 estagios e um terceiro que é o Fetch mas este é um stagio "transparente" que significa que usa o tempo do primeiro estágio, no meu o Fetch foi significamente alterado, passou a ser muito mais complexo pois supporta não só instruções normais de uma word (16 bits) mas suporta tambem instruções de 8 bits que permite reduzir o numero de vezes que a memoria tem de ser acedida. As outras pipelines têm como principal diferença a mudança das roms para roms maiores e mais rapidas.

### Registos Gerais

### SPI

Desenhei de raiz um controlador de SPI com 16 bits de largura, supporta todos os modos, chip select manual, supporta 16 slaves diferentes

### Multiplicador

Obrigado pela idea de RJ45 de usar uma tabela de quadrados, em que A*B = F(A+B)-F(A-B) com F(x)=x²/4, usando 2 grupos de roms para guardar as tabelas 2 sumadores de 16 bits e 1 de 32 bits, permitiu-me desenhar um multiplicador que suporta 

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
|Arquitetura            | James   | Bruno    |
|-----------------------|:-------:|:--------:|
|     Precisão base     |  8 bits |  16 bits |
| Pipeline              | 2 estagios | 2 estagios + Fetch  |
| Registos gerais       |    4    |    8     |
| Registos de Endereço  |    5    |    6     |
| Memoria               |    2¹⁶ endereços * 8 bits  |    2²⁴ endereços * 16 bits  |
| Instruções            |    8 bits   |  8 bits e 16 bits  |
| ALU                   |  Adição, subtração <br> shift em ambas as direções<br> <br> Operações logicas <br> <br>imediatos de 8 bits<br> | Adição, subtração <br> shift L/R 1-4 bits, com rotate<br> <br> Operações logicas <br> <br>imediatos de 16 bits<br> <br>multiplicador (com e sem sinal)<br>  |
| IO                 | Uart  |  Uart <br>SPI<br> <br>Ethernet<br>  |
| Serie de Chips        | HCT  |  Fast (F)  |
{{</table>}}

