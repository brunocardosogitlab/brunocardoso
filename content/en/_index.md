---
title: "Bruno Cardoso"
cascade:
  featured_image: '/UC_V_FundoClaro-negro.png'

##description: "Projects"
---
Electronic Engineering and Computer Science Student

#### -Personal Projects
* 16 bit Homebrew Computer
Passion project loosely based on the work of James Sharman,
Maintaining the same base structure but improving every block and adding more features and functionality.

#### -University Projects
* For Project I
* Computer Networks 
* For Project II




This was a Project made for the Project II course in the University of Coimbra,
it consisted of making a smart current sensor which detected problems in electrical motors
so that they could be solved before they led to catastrofic failure and factory downtime.

