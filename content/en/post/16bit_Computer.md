---
date: 2022-06-16T10:58:08-04:00
featured_image: "/images/Pope-Edouard-de-Beaumont-1844.jpg"
tags: ["scene"]
title: "16 Bit Computer"
---
# Introduction

Computer architecture has been an area of interest for a long time, and ever since i saw [Ben Eater](https://eater.net/)'s videos
on youtube on the process of making a very simple 8 bit computer called SAP-1 (Simple as possible 1), i got the itch to make my own.
Initially i was simpply upgrading the SAP-1, with more registers a better ALU (74ls181 chip) and more memory using 74LS series chips,
But while doing this i found [James Bates](https://www.youtube.com/user/jimmyb998) which had even more upgrades and thus the plans were updated,
while waiting for parts i discovered [Bill Buzbee](http://www.homebrewcpu.com/about_me.htm) and his [MAGIC-1](http://www.homebrewcpu.com/) 
which was powerful enough to run the Minix Operating system based on Unix, and host it's own web page to the internet. At the time such an architecture felt out of reach. 
At Last i found [James Sharman](https://www.youtube.com/c/weirdboyjim) which was making something different, a pipelined 8 bit computer,
with a much more complex architecture than the SAP-1, but easier to understand than MAGIC-1, in large part thanks to the hundreds of videos showing step by step how the architecture was made,
what choices influenced this decisions, etc. So my plans were again updated and i started planning a cpu around his.

## James's JAM-1 8 bit computer


8 bit architecture, separated into 2 stages with the first doubling as a fetch, with 4 general purpose registers, and dedicated 16 bit memory address Registers,
A fairly complete ALU with Addition, subtraction, bitwise logic operations, and shifts (left / right). Its memory system is byte addressed and the whole address range is filled by SRAM, with contents being copied from ROM to Ram at startup.
One of its most distinguishing features is the addition of a video subsystem, adding direct support for
tiled based graphics with 24 bit color thanks to a pallete, as well as horizontal and vertical hardware scrolling. 
thanks to being pipelined and it running confortably at 4mhz, its Theoretical performance is 4MIPS,
with real performance around 3 MIPS (after accounting for pipeline bubbles), placing it well above the
performance of early personal computers such as a commodore 64 (specifically close to 7 times faster).
The majority of the computer was made using HC/HCT series chips, using smt packages.



# Architeture

Before starting this section something i have to make clear, to quote a wise men "There are no solutions only trade-offs", meaning that my changes to james's initial design are simply a result
of having different design goals. Having just mentioned design goals, what were mine?, well in resume it was absolute performance, while still being feasable, and mostly importantly keeping all components in DIP packages because they simply look good.
With this in mind, i maintained james's high level architecture and focused instead on: increasing clockspeeds, Reducing pipeline bubbles, and making each instruction do more.

Expanding on the design goals: 



* Increasing clockspeeds

    10Mhz clock speed goal, by moving to F series logic, and faster ROMs.

* Reducing pipeline bubbles

    Adding 8 bit short instructions alongside 16 bit instructions reducing the fetch bandwidth needs    
    
* making each instruction do more

    * 16 bit wide system - To reduce the need to waste multiple instructions in a effort to have enough precision, 
    
    * add more GPRs ( 8 ) - To reduce the relience on the stack,
    
    * add hardware stack - To further reduce memory pressure,

    * Improved Shifter unit, 

    * adding a integer Multiplier,
    
    * SPI controller ( future ENC28J60 ethernet controller addition),

    * 24 bit memory address space,



### Base - Logic family used - introducing FAST

A little history lesson first, the basis for most discrete logic chips used to this day,
have its basis on the SN7400 series launched by Texas Instruments all the way back in 1966,
which started using Transistor Transistor Logic or TTL, over the years many variants were made,
some reducing power usage (Low power schottky), some moving to CMOS transistors (HC / HCT), and 
more modern ones moving away from 5V logic levels and into lower voltage standards.
From the various one standed out to me, simply called FAST, one of the last based around bi-polar
transistors and boasting impressive propagation (eg: 10.5ns for a SN74F283 vs 69 ns for a 74HCT283) delays and fast rise times ( ~2 ns), making it ideal to help achieve my goals of increasing clocks,
to counter balance its performance its power consumption is many times higher than other logic families.


### SPI

Designed a SPI controller from scratch, its 16 bits wide, supports the 4 different modes, automatic chip select with manual override,
supports 16 different slave devices,  

### Multiplier
Multipliers are either slow of order N cycles or extremely large for single cycle versions, specially for 16 bit systems, first attempts (based on Wallace tree) were
leading to circuits that were too large, but thanks to [RJ45](https://www.youtube.com/c/rj45Creates), he pointed out that one can use a table of squares where
A*B = F(A+B)-F(A-B) with F(x)=x²/4, with this method instead of a large group of AND gates and adders, it uses 2 large roms each 4Mbits and 32bit wide, and 3 adders 2 16 bits wide and 1 32 bits wide.
The final design uses only 38 chips (using 16bit roms), occupies 2 100x100mm boards and completes a 16 bit multiply in 2 cycles (pipelined).
It supports signed and unsigned numbers, and has rom space for other tables if needed.

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
|Architecture           | James   | Bruno    |
|-----------------------|:-------:|:--------:|
|     Base Precision    |  8 bits |  16 bits |
| Pipeline              | 2 Stages | 2 Stages + Fetch  |
| General Registers     |    4    |    8     |
| Address Registers     |    5    |    6     |
| SRAM Memory           |    2¹⁶ addresses * 8 bits  |    2²⁴ addresses * 16 bits  |
| Instructions          |    8 bits   |  8 bits e 16 bits  |
| ALU                   |  Addition, substraction <br> shift Left / Right<br> <br> Logical Operations <br> <br> 8 bit immidiate<br> | Addition, substraction <br> shift or Rotate Left / Right by 1-4 bits<br> <br> <br> Logical Operations <br> <br>16 bit immidiates<br> <br>multiplier Signed/ Unsigned <br>  |
| IO                 | Uart  |  Uart <br>SPI<br> <br>Ethernet<br>  |
| Chips Family          | HCT  |  Fast (F)  |
{{</table>}}




Mantive a mesma arquitetura geral do James, mas em cada bloco modifiquei ao maximo,
### base - Chips usados

  Em 1966 Texas Instruments lançou a serie de chips logicos SN7400 que rapidamente se tornou num fenomeno sucesso de tal modo que ainda hoje chips são feitos baseados nessa serie, esta usava logica transistor transistor (TTL), ao longo dos anos multiplas versões desta serie foram feitas, umas reduzindo o consumo de energia, outras permitindo mais performance, de destaque a serie LS (schottky de baixo consumo) o tipo mais popular com consumo mais baixo que a serie original, HC (usando CMOS de alta velocidade) lançados nos anos 80, a mais importante para este artigo a serie Fast (74F) lançada em 1978, a versão mais rapida (das de 5 voltes).
James usou a versão HCT (o T significa que supporta os niveis de tensão TTL), uma familia que usa muito menos corrente por usar mosfets, eu quis focar-me em maximizar o performance por isso, em troca de muito mais corrente usei a familia Fast, baseada em TTL que não só tem tempos de propagação bastante mais baixos, tambem tem tempos de ascensão na gama de ~1-2 ns, um exemplo da diferença 69 ns para os 74HCT283, a versão Fast 74F283 tem tempos de propagação de apenas 10.5 ns.

### Precisão base

Passei de 8 bits para 16 bits, isto embora signifique que tenha que duplicar o numero de chips usados, 16 bits significa muito mais precisão e significa muito menos necessidade de aumentar a precisão usando multiplas words.

### Pipeline

No cpu do James ele tem 2 estagios e um terceiro que é o Fetch mas este é um stagio "transparente" que significa que usa o tempo do primeiro estágio, no meu o Fetch foi significamente alterado, passou a ser muito mais complexo pois supporta não só instruções normais de uma word (16 bits) mas suporta tambem instruções de 8 bits que permite reduzir o numero de vezes que a memoria tem de ser acedida. As outras pipelines têm como principal diferença a mudança das roms para roms maiores e mais rapidas.

### Registos Gerais

### SPI

Designed a SPI controller from scratch, its 16 bits wide, supports the 4 different modes, automatic chip select with manual override,
supports 16 different slave devices,  


### Multiplier
Multipliers are either slow of order N cycles or extremely large for single cycle versions, specially for 16 bit systems, first attempts (based on Wallace tree) were
leading to circuits that were too large, but thanks to [RJ45](https://www.youtube.com/c/rj45Creates), he pointed out that one can use a table of squares where
A*B = F(A+B)-F(A-B) with F(x)=x²/4, with this method instead of a large group of AND gates and adders, it uses 2 large roms each 4Mbits and 32bit wide, and 3 adders 2 16 bits wide and 1 32 bits wide.
The final design uses only 38 chips (using 16bit roms), occupies 2 100x100mm boards and completes a 16 bit multiply in 2 cycles (pipelined).
It supports signed and unsigned numbers, and has rom space for other tables if needed.

{{< table table_class="table table-striped table-bordered" thead_class="table-dark" >}}
|Architecture           | James   | Bruno    |
|-----------------------|:-------:|:--------:|
|     Base Precision    |  8 bits |  16 bits |
| Pipeline              | 2 Stages | 2 Stages + Fetch  |
| General Registers     |    4    |    8     |
| Address Registers     |    5    |    6     |
| SRAM Memory           |    2¹⁶ addresses * 8 bits  |    2²⁴ addresses * 16 bits  |
| Instructions          |    8 bits   |  8 bits e 16 bits  |
| ALU                   |  Addition, substraction <br> shift Left / Right<br> <br> Logical Operations <br> <br> 8 bit immidiate<br> | Addition, substraction <br> shift or Rotate Left / Right by 1-4 bits<br> <br> <br> Logical Operations <br> <br>16 bit immidiates<br> <br>multiplier Signed/ Unsigned <br>  |
| IO                 | Uart  |  Uart <br>SPI<br> <br>Ethernet<br>  |
| Chips Family          | HCT  |  Fast (F)  |
{{</table>}}


